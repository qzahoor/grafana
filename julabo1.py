import serial
import time
import matplotlib.pyplot as plt


# Initialize serial communication with the Julabo Chiller
ser = serial.Serial('COM10', 9600, timeout=1)
time.sleep(2) # wait for the Julabo Chiller to initialize
ser.write("out_mode_05 1\r\n".encode("utf-8"))  #turns on the chiller julabo A80

# Set the temperature set point to 18 degrees Celsius
ser.write(b"out_sp_00 18\r")




# Initialize the temperature and time arrays
temperatures = []
times = []

# Set the temperature set point to a range of 18 to 22 degrees Celsius using a for loop
for temperature in range(18, 21):
    # Set the temperature set point
    ser.write("out_sp_00 {}\r".format(temperature).encode())
    #time.sleep(300)
    ser.write("in_sp_00\r".format(temperature).encode())
    print("setpoint is", (temperature))
    time.sleep(300)

    # Record the temperature readings every 10 minutes
    for i in range(3):
        # Read the temperature value from the Julabo Chiller bath temperature
        ser.write(b"in_pv_00?\r")
        temperature1 = float(ser.readline().decode().strip())
        print("Actual bath temperature is", (temperature))

        # Append the temperature and time values to their respective arrays
        temperatures.append(temperature)
        times.append(time.time())

        # Wait for 10 minutes
        time.sleep(600)


# Write the temperature and time data to a text file
with open("temperature_data1.txt", "w") as f:
    for temperature, time in zip(temperatures, times):
        f.write("{} {}\n".format(time, temperature))

# Plot the temperature data over time
plt.plot(times, temperatures)
plt.title("Temperature variation with time of JulaboA80")
plt.xlabel("Time (s)")
plt.ylabel("Temperature (C)")
plt.show()
# Close the serial connection with the Julabo Chiller

ser.write("out_mode_05 0/r/n".encode("utf-8"))

ser.close()
print("chiller is closed")
